///This is some serious shit. This is about building a command-line tool
/// If you're looking for some information related to what we learned prior -
/// check the projects rust_mini_proj_1
/// or adder. These projects are the culmination of what I learned.
/// They cover the important things like generics and how to import
/// and utilise files and modules in directories

//bring it env into scope
//env::args means that it's nested in more than one module. Whereas using a . means that you're calling the field in the class
extern crate minigrep;
use std::{env, process};
use minigrep::{Config};

fn main() {
	//turns the iterator into a Vector because rust doesn't infer collections based on its kind
	
	//Need to return std::env::Args
	let config = Config::new(env::args()).unwrap_or_else(|err| {
		eprintln!("Problem parsing arguments: {}", err);
		process::exit(1);
	});
	
	//Let Err over unwrap_or_else is
	if let Err(e) = minigrep::run(config) {
		eprintln!("Application error: {}", e);
		process::exit(1);
	}
}