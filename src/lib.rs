use std::fs::File;
use std::error::Error;
use std::io::Read;
use std::env;

//1. Return is Result<(), Box<Error>>
// Keeping 0 as the return value for Ok case
//2. Error returns a type that implements Error, but not needed to be specified
//3. () means that the success type is () Meaning we need to wrap the unit in the Ok value
pub fn run(config: Config) -> Result<(), Box<dyn Error>> {
	let mut f = File::open(config.filename)?;
	
	let mut contents = String::new();
	f.read_to_string(&mut contents)?;
	
	let results = if config.case_sensitive {
		search(&config.query, &contents)
	} else {
		search_case_insensitive(&config.query, &contents)
	};
	
	for line in results {
		println!("{}", line);
	}
	//Calls run for its side effects only. Doesn't return a value we need
	Ok(())
}

pub struct Config {
	pub query: String,
	pub filename: String,
	pub case_sensitive: bool,
}

impl Config {
	//lifetime 'static str lifetime is the string literal
	pub fn new(mut args: std::env::Args) -> Result<Config, &'static str> {
		args.next();
		let query = match args.next() {
			Some(arg) => arg,
			None => return Err("Where is the query mate?")
		};
		
		let filename = match args.next() {
			Some(arg) => arg,
			None => return Err("Where is the file name fuckhead?")
		};
		
		let case_sensitive = env::var("CASE_INSENSITIVE").is_err();
		Ok(Config { query, filename, case_sensitive })
	}
}

pub fn search<'a>(query: &str, contents: &'a str) -> Vec<&'a str> {
	contents.lines()
	        .filter(|line| line.contains(query))
	        .collect()
}

pub fn search_case_insensitive<'a>(query: &str, contents: &'a str)
                                   -> Vec<&'a str> {
//create shadow variable and store as lowercase copy
	let query = query.to_lowercase();
	let mut results = Vec::new();
	for line in contents.lines() {
		if line.to_lowercase().contains(&query)
		{
			results.push(line);
		}
	}
	results
}
