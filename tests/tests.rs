#[cfg(test)]
mod test {
	use minigrep::{search, search_case_insensitive};
	
	#[test]
	fn one_result() {
		let query = "duct";
		let contents = "/
Rust:
safe, fast, productive.
Pick three.";
		
		assert_eq!(vec!["safe, fast, productive."],
		           search(query, contents)
		);
	}
	
	#[test]
	fn case_sensitive() {
		let query = "duct";
		let contents = "/
Rust:
safe, fast, productive.
Pick three.
Duct";
		assert_eq!(
			vec!["safe, fast, productive."],
			search(query, contents)
		);
	}
	
	#[test]
	fn case_insensitive() {
		let query = "ruSt";
		let contents = "/
Rust:
safe, fast, productive.
Pick three.
Trust me.";
		assert_eq!(
			vec!["Rust:", "Trust me."],
			search_case_insensitive(query, contents)
		);
	}
	
}
